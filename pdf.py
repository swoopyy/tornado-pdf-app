import PyPDF2
from wand.image import Image
import io


def pdf_page_to_png(page):
    dst_pdf = PyPDF2.PdfFileWriter()
    dst_pdf.addPage(page)


    pdf_bytes = io.BytesIO()
    dst_pdf.write(pdf_bytes)
    pdf_bytes.seek(0)

    img = Image(file=pdf_bytes)
    return img.make_blob(format="png")


def pdf_to_png_pages(blob):
    bytes = io.BytesIO()
    bytes.write(blob)
    pdf = PyPDF2.PdfFileReader(bytes)
    pages = []
    for page in pdf.pages:
        png = pdf_page_to_png(page)
        pages.append(png)
    return pages

