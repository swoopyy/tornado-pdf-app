from peewee import *
from datetime import datetime
from hashlib import sha1
from random import random
from playhouse.sqlite_ext import SqliteExtDatabase

database = SqliteExtDatabase('app.db')


def get_hexdigest(salt, raw_password):
    data = salt + raw_password
    return sha1(data.encode('utf8')).hexdigest()


@database.func()
def make_password(raw_password):
    salt = get_hexdigest(str(random()), str(random()))[:5]
    hsh = get_hexdigest(salt, raw_password)
    return '%s$%s' % (salt, hsh)


@database.func()
def check_password(raw_password, enc_password):
    salt, hsh = enc_password.split('$', 1)
    return hsh == get_hexdigest(salt, raw_password)


class BaseModel(Model):
    class Meta:
        database = database


class User(BaseModel):
    email = CharField(unique=True)
    password = CharField()


class File(BaseModel):
    user = ForeignKeyField(User)
    name = CharField()
    date = DateTimeField(default=datetime.now())
    file = BlobField()

    @classmethod
    def get_tree(cls):
        files = File.select()
        return [{
            "file": file,
            "pages": Page.filter(file=file)
        } for file in files]

    class Meta:
        order_by = ('date',)


class Page(BaseModel):
    file = ForeignKeyField(File)
    name = CharField()
    page = BlobField()



