import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.options
import os.path
from peewee import fn
from pdf import pdf_to_png_pages

from tornado.options import define, options

from models import User, Page, File

define("port", default=8888, help="run on the given port", type=int)


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")


class MainHandler(BaseHandler):
    template = 'index.html'

    @tornado.web.authenticated
    def get(self):
        data = {
            "error": "",
             "tree": File.get_tree()
        }
        self.render(self.template, data=data)

    @tornado.web.authenticated
    @tornado.gen.coroutine
    def post(self):
        if 'filearg' not in self.request.files:
            data = {
                "error": "You didn't choose a file",
                "tree": File.get_tree()
            }
            self.render(self.template, data=data)

        fileinfo = self.request.files['filearg'][0]
        if fileinfo.content_type != 'application/pdf':
            data = {
                "error": "Not a pdf",
                "tree": File.get_tree()
            }
            self.render(self.template, data=data)
        else:
            user = User.get(email=self.current_user)
            file = File.create(
                user=user,
                name=fileinfo['filename'],
                file=fileinfo['body']
            )
            pages = pdf_to_png_pages(file.file)
            num = 1
            for page in pages:
                Page.create(
                    file=file,
                    page=page,
                    name="Page%s.png" % num
                )
                num += 1
            data = {
                "error": "",
                "tree": File.get_tree()
            }
            self.render(self.template, data=data)


class LoginHandler(BaseHandler):
    template = 'login.html'

    @tornado.gen.coroutine
    def get(self):
        data = {
            "email": "",
            "error": ""
        }
        self.render(self.template, data=data)

    @tornado.gen.coroutine
    def post(self):
        email = tornado.escape.xhtml_escape(self.get_argument("email"))
        password = tornado.escape.xhtml_escape(self.get_argument("password"))
        try:
            User.select().where((User.email == email) & (fn.check_password(password, User.password) == True)).get()
            self.set_secure_cookie("user", email)
            self.redirect(self.reverse_url("main"))
        except User.DoesNotExist:
            data = {
                "email": email,
                "error": "Invalid email or password"
            }
            self.render(self.template, data=data)


class SignupHandler(BaseHandler):
    template = 'signup.html'

    @tornado.gen.coroutine
    def get(self):
        data = {
            "email": "",
            "error": ""
        }
        self.render(self.template, data=data)

    @tornado.gen.coroutine
    def post(self):
        email = tornado.escape.xhtml_escape(self.get_argument("email"))
        password = tornado.escape.xhtml_escape(self.get_argument("password"))
        password_confirmation = tornado.escape.xhtml_escape(self.get_argument("password_confirmation"))
        if password != password_confirmation:
            data = {
                "email": email,
                "error": "Passwords do not match",
            }
            self.render(self.template,  data=data)
            return
        try:
            User.get(User.email == email)
            data = {
                "email": email,
                "error": "User with such email already exists",
            }
            self.render(self.template, data=data)
            return
        except User.DoesNotExist:
            User.insert(email=email, password=fn.make_password(password)).execute()
            self.set_secure_cookie("user", email)
            self.redirect(self.reverse_url("main"))


class LogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect(self.get_argument("next", self.reverse_url("main")))


class FileDownloaderHandler(BaseHandler):
    @tornado.web.authenticated
    @tornado.gen.coroutine
    def get(self, file_id):
        try:
            file = File.get(id=file_id)
            self.set_header('Content-Type', 'application/pdf')
            self.set_header('Content-Disposition', 'attachment; filename=' + file.name)
            self.write(str(file.file))
        except File.DoesNotExist:
            self.redirect(self.reverse_url("main"))

class PageDownloaderHandler(BaseHandler):
    @tornado.web.authenticated
    @tornado.gen.coroutine
    def get(self, page_id):
        try:
            page = Page.get(id=page_id)
            self.set_header('Content-Type', 'image/png')
            self.set_header('Content-Disposition', 'attachment; filename=' + page.name)
            self.write(str(page.page))
        except File.DoesNotExist:
            self.redirect(self.reverse_url("main"))


class Application(tornado.web.Application):
    def __init__(self):
        base_dir = os.path.dirname(__file__)
        settings = {
            "cookie_secret": "o40V3vqhqSLJAiXIioDfogjY0UdXnvzR",
            "login_url": "/login",
            'template_path': os.path.join(base_dir, "templates"),
            'static_path': os.path.join(base_dir, "static"),
            'debug':True,
            "xsrf_cookies": True,
        }
        
        tornado.web.Application.__init__(self, [
            tornado.web.url(r"/", MainHandler, name="main"),
            tornado.web.url(r"/signup", SignupHandler, name="signup"),
            tornado.web.url(r'/login', LoginHandler, name="login"),
            tornado.web.url(r'/logout', LogoutHandler, name="logout"),
            tornado.web.url(r'/file/(\d*)', FileDownloaderHandler, name="file"),
            tornado.web.url(r'/page/(\d*)', PageDownloaderHandler, name="page")

        ], **settings)

def main():
    tornado.options.parse_command_line()
    Application().listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()

