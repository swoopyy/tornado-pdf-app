import models

def create_tables():
    models.database.connect()
    models.database.create_tables([models.User, models.File, models.Page], True)

if __name__ == "__main__":
    create_tables()
